
function App(options) {
    this.options = options

    // Form css selector
    this.options.form = this.options.form || 'form'

    // jquery form
    var form = this.form = $(this.options.form)

    if (!this.form.length) {
        throw new Error('Cant find form ' + this.options.form)
    }

    form.get(0).elements.company.addEventListener('invalid', function (event) {
        console.log('invalid', this, this.validity)
        // $('<div class="invalid-feedback">Field is required</div>').insertAfter(this)
    })

    var self = this;
    this.form.on('submit', function (event) {
        var submit = event.target;
        event.preventDefault()
        //    form.find('.invalid-feedback').remove();

        this.checkValidity()
        form.addClass('was-validated')

        var record = self.getFormData(form)
        self.addRecord(record)

        console.log(this)
    })

    this.form.find('#extra').on('change', function (event) {
        // usun pole
        if (!this.checked) {
            $('#extra_field').remove()
            return;
        };
        // dodaj pole 
        var tpl = $(options.form_extra.field_template).get(0).content.cloneNode(true)
        tpl = $(tpl)
        tpl.find('.form-group').attr('id', 'extra_field')
        tpl.find('label').text('Extra')

        tpl.insertBefore(form.children().last())
    })

    this.getFormData = function(form){
        return form.find('input,select,textarea').toArray().reduce( function( record, field){
            if(!field.id){ return record }
            // get value from input, checked from checkbox
            record[ field.id ] = (['checkbox','radio'].indexOf(field.type) != -1 )? field.checked : field.value;
            return record;
        },{})
    }

    this.addRecord = function(record){
        var records = $('table').find('tbody')
        var tr =  $('<tr>')

        tr.append('<td>'+ (records.children().length + 1) +'.</td>')
        tr.append('<td>'+record.company+'</td>')
        tr.append('<td>'+record.category+'</td>')
        tr.append('<td>'+record.income+' $</td>')

        records.append(tr )
    }
}