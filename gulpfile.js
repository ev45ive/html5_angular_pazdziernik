// https://github.com/gulpjs/gulp/blob/master/docs/API.md
var gulp = require('gulp')
var concat = require('gulp-concat')
var sass = require('gulp-sass')
var babel = require("gulp-babel");

gulp.task("babel", function () {
  return gulp.src("./docs/objekty.js")
    .pipe(babel())
    .pipe(gulp.dest("dist"));
});

gulp.task('sass', function(){

    gulp.src('./src/**/*.scss')
        .pipe(sass({
            // outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(gulp.dest('./dist/'))
})


gulp.task('watch',function(){

    gulp.watch('./src/**/*.scss',['sass'])
})


gulp.task('copy',function(){

    gulp.src([
        './src/**/*.css',
        '!./src/**/print.css'
    ])
        .pipe(concat('styles.css'))
        .pipe( gulp.dest('./dist/'))
})


gulp.task('default', function(){
    console.log('Hello from Gulp!')
})