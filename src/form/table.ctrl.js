
// Load exintisng income_form module
angular.module('income_form')

.controller('incomeTable',function($scope, IncomeRecords){

$scope.companies = IncomeRecords.getData()

$scope.remove = function(record){
    IncomeRecords.delete(record.id);
}

$scope.refresh = function(){
    IncomeRecords.fetchRecords()
    .then(function(records){
        // $scope.companies. = records;
    })
}

$scope.select = function(company){
    IncomeRecords.select(company)
}

$scope.refresh()
})